import matplotlib.pyplot as plt
import networkx as nx

# Definicion de la clase Nodo del Arbol B+
class BPlusTreeNode:
    def __init__(self, is_leaf=False):
        #Inicializa el nodo del arbol B+
        self.is_leaf = is_leaf  # True si es un nodo hoja
        self.keys = []  # Lista de claves en el nodo
        self.children = []  # Lista de hijos, ya sea los nodos hijos o punteros a hojas

# Definicion de la clase del Arbol B+
class BPlusTree:
    def __init__(self, max_keys=4):
        #Inicializa el arbol B+
        self.root = BPlusTreeNode(is_leaf=True)  # Nodo raiz, es inicialmente una hoja
        self.max_keys = max_keys  # Maximo numero de claves por nodo que serian 4

    def insert(self, key):
        #Inserta una clave en el arbol B+
        root = self.root
        #Si la raiz esta llena, se divide en dos nodos
        if len(root.keys) == self.max_keys:
            new_root = BPlusTreeNode()  # Crea al nuevo nodo raiz
            new_root.children.append(self.root)  # El nuevo nodo raiz tiene un hijo que es la antigua raiz
            self.split_child(new_root, 0)  # Dividir el hijo
            self.root = new_root  # Actualizar la raiz
        self._insert_non_full(self.root, key)  # Insertar la clave en el nodo adecuado

    def _insert_non_full(self, node, key):
        #Inserta una clave en un nodo que no este lleno
        if node.is_leaf:
            self._insert_into_leaf(node, key)  # Inserta directamente en el nodo hoja
        else:
            i = self._find_child(node, key)  # Encuentra el hijo adecuado para descender
            child = node.children[i]
            if len(child.keys) == self.max_keys:
                self.split_child(node, i)  # Si el hijo esta lleno lo divide
                if key > node.keys[i]:
                    i += 1  # Determina si se debe insertar en el nuevo nodo
            self._insert_non_full(node.children[i], key)  # Inserta en el nodo adecuado

    def _insert_into_leaf(self, leaf, key):
        #Inserta una clave en un nodo hoja
        leaf.keys.append(key)  # Añade la clave a la lista de claves del nodo hoja
        leaf.keys.sort()  # Ordena las claves del nodo hoja

    def _find_child(self, node, key):
        #Encuentra el hijo para descender durante la insercion
        for i, item in enumerate(node.keys):
            if key < item:
                return i  # Retorna el indice del hijo donde se debe insertar la clave
        return len(node.keys)  # Si la clave es mayor que todas las que existen entonces retorna el ultimo indice

    def split_child(self, parent, index):
        #Divide un hijo lleno en dos nodos
        full_node = parent.children[index]
        mid = self.max_keys // 2  # Punto medio para la division
        split_key = full_node.keys[mid]
        
        #Crea nodos izquierdo y derecho
        left_node = BPlusTreeNode(is_leaf=full_node.is_leaf)
        right_node = BPlusTreeNode(is_leaf=full_node.is_leaf)
        
        #Asigna claves e hijos a los nodos izquierdo y derecho
        left_node.keys = full_node.keys[:mid]
        right_node.keys = full_node.keys[mid + 1:]
        if not full_node.is_leaf:
            left_node.children = full_node.children[:mid + 1]
            right_node.children = full_node.children[mid + 1:]

        #Actualizar el padre con la nueva clave y los nuevos hijos
        parent.keys.insert(index, split_key)
        parent.children[index] = left_node
        parent.children.insert(index + 1, right_node)

    def visualize(self):
        #Visualiza el arbol B+ utilizando matplotlib y networkx
        G = nx.DiGraph()  # Crear un grafo dirigido
        labels = {}  # Diccionario para las etiquetas de los nodos
        self._add_edges(self.root, G, labels)  # Agregar aristas y nodos al grafo
        pos = nx.spring_layout(G)  # Layout para la disposicion de los nodos
        nx.draw(G, pos, with_labels=True, labels=labels, node_size=2000, node_color='skyblue', font_size=10)  # Dibujar el grafo
        nx.draw_networkx_edge_labels(G, pos, edge_labels={(u, v): d['label'] for u, v, d in G.edges(data=True)})  # Dibujar etiquetas de aristas
        plt.show()  # Mostrar el grafo

    def _add_edges(self, node, G, labels, parent=None, parent_index=None):
    # Agrega aristas para la visualizacion del arbol
        node_id = id(node)  # Identificador unico para el nodo
        labels[node_id] = ','.join(map(str, node.keys))  # Etiqueta del nodo conecta con sus claves
        if parent is not None:
            G.add_edge(parent, node_id, label=parent_index)  # Agregar arista del padre al nodo actual
        if node_id not in G.nodes:  # Verificar si el nodo ya ha sido agregado al grafo
            G.add_node(node_id)  # Agregar el nodo al grafo
        for i, child in enumerate(node.children):
            self._add_edges(child, G, labels, node_id, i)  # Recursivamente va agregando las aristas de los hijos


# Definicion de la clase Nodo del arbol Binario
class Node:
    def __init__(self, val):
        self.val = val  # Valor del nodo
        self.left = None  # Hijo izquierdo
        self.right = None  # Hijo derecho

# Definicion de la clase del arbol Binario
class BinaryTree:
    def __init__(self):
        self.root = None  # Nodo raiz del arbol

    def insert(self, val):
        if not self.root:
            self.root = Node(val)  # Si el arbol esta vacio, crear la raiz
        else:
            self._insert(self.root, val)  # Insertar en el lugar adecuado

    def _insert(self, node, val):
        if val < node.val:
            if not node.left:
                node.left = Node(val)  # Si no hay hijo izquierdo lo insertara aqui
            else:
                self._insert(node.left, val)  # Continua buscando en el subarbol izquierdo
        else:
            if not node.right:
                node.right = Node(val)  # Si no hay hijo derecho lo insertara aqui
            else:
                self._insert(node.right, val)  # Continua buscando en el subarbol derecho

    def search(self, val):
        return self._search(self.root, val)  # Buscar el valor desde la raiz

    def _search(self, node, val):
        if not node or node.val == val:
            return node  # Retorna el nodo si lo encuentra o si llega a un nodo nulo entonces
        if val < node.val:
            return self._search(node.left, val)  # Busca en el subarbol izquierdo
        return self._search(node.right, val)  # Busca en el subarbol derecho

    def update(self, old_val, new_val):
        #Actualiza el valor de un nodo en el arbol binario
        node = self._search(self.root, old_val)  # Buscar el nodo con el valor antiguo
        if node:
            node.val = new_val  # Si se encuentra el nodo actualizara su valor por el nuevo
            print(f"Value {old_val} updated to {new_val}.")
        else:
            print(f"Value {old_val} not found in the tree.") # Si no lo encuentra le notificara al usuario que no existe

    def inorder_traversal(self, node, result=None):
        #Realiza un recorrido inorden del arbol y almacena los nodos en una lista.#
        if result is None:
            result = []  # Lista de resultados
        if node:
            self.inorder_traversal(node.left, result)
            result.append(node.val)                  # Va agregando los nodos en orden al arreglo de result
            self.inorder_traversal(node.right, result)
        return result

    def build_balanced_tree(self, nodes):
        #Construye un arbol balanceado a partir de una lista de nodos ordenados.#
        if not nodes:
            return None
        mid = len(nodes) // 2
        root = Node(nodes[mid]) 
        root.left = self.build_balanced_tree(nodes[:mid])   # Llama de manera reiterativa build_balance_tree para que acomode los valores de los indices 
        root.right = self.build_balanced_tree(nodes[mid+1:])# de la lista izquierda y la derecha
        return root

    def balance(self):
        #Balancea el arbol binario.#
        nodes = self.inorder_traversal(self.root)  # Obtener todos los nodos en inorden
        self.root = self.build_balanced_tree(nodes)  # Reconstruir el arbol balanceado
        print("Binary tree balanced.")

    def visualize(self):
        if not self.root:
            print("Tree is empty.")
            return

        plt.figure(figsize=(10, 6))  # Crea figura de matplotlib
        pos = self._get_positions(self.root)  # Obtener posiciones de los nodos
        self.draw_tree(self.root, pos)  # Dibuja el arbol
        plt.axis('off')  # Oculta los ejes x, y 
        plt.show()  # Muestra el arbol

    def _get_positions(self, node, x=0, y=0, pos=None, layer=1):
        if pos is None:
            pos = {node: (x, y)}  # Posicion inicial para el nodo raiz
        else:
            pos[node] = (x, y)  # Actualizar posicion del nodo
        if node.left:
            pos.update(self._get_positions(node.left, x - 1 / layer, y - 1, pos, layer + 1))  # Posicion del hijo izquierdo
        if node.right:
            pos.update(self._get_positions(node.right, x + 1 / layer, y - 1, pos, layer + 1))  # Posicion del hijo derecho
        return pos

    def draw_tree(self, node, pos, parent=None):
        x, y = pos[node]  # Posicion del nodo actual
        plt.text(x, y, str(node.val), ha='center', va='center', bbox=dict(facecolor='skyblue', edgecolor='black', boxstyle='round,pad=0.5'))  # Dibujar el nodo
        if parent:
            px, py = pos[parent]
            plt.plot([px, x], [py, y], 'k-')  # Dibujar la arista al padre
        if node.left:
            self.draw_tree(node.left, pos, node)  # Dibujar subarbol izquierdo
        if node.right:
            self.draw_tree(node.right, pos, node)  # Dibujar subarbol derecho

class Menu:
    def __init__(self):
        self.stack = []  # Inicializa la pila vacia
        self.queue = []  # Inicializa la cola vacia
        self.binary_tree = BinaryTree()  # Crea el arbol binario
        self.b_plus_tree = BPlusTree(max_keys=4)  # Crea el arbol B+ con maximo 4 claves por nodo
        self.graph = nx.Graph()  # Crea un grafo vacio

    def main_menu(self):
        while True:
            print("\n Data Structures ")
            print("1. Stack")
            print("2. Queue")
            print("3. Binary Tree")
            print("4. B+ Tree")
            print("5. Graph (BFS, DFS)")
            print("6. Dijkstra's Algorithm")
            print("0. Exit")
            choice = input("Enter your choice: ")

            if choice == '1':
                self.stack_menu()
            elif choice == '2':
                self.queue_menu()
            elif choice == '3':
                self.binary_tree_menu()
            elif choice == '4':
                self.b_plus_tree_menu()
            elif choice == '5':
                self.graph_menu()
            elif choice == '6':
                self.dijkstra_menu()
            elif choice == '0':
                break
            else:
                print("Invalid choice. Please try again.")

    def stack_menu(self):
        while True:
            print("\nStack Menu")
            print("1. Push")
            print("2. Pop")
            print("3. Peek")
            print("4. Display Stack")
            print("0. Back to Main Menu")
            choice = input("Enter your choice: ")

            if choice == '1':
                value = input("Enter value to push: ")
                self.stack.append(value)
                print(f"Value {value} pushed to stack.")
            elif choice == '2':
                if not self.stack: 
                    print("Stack is empty.")
                else:
                    value = self.stack.pop()# Elimina el tope de la pila
                    print(f"Value {value} popped from stack.")
            elif choice == '3':
                if not self.stack:
                    print("Stack is empty.")
                else:
                    print(f"Top value is {self.stack[-1]}.")
            elif choice == '4':
                print("Stack:", self.stack)
            elif choice == '0':
                break
            else:
                print("Invalid choice. Please try again.")

    def queue_menu(self):
        while True:
            print("\nQueue Menu")
            print("1. Enqueue")
            print("2. Dequeue")
            print("3. Display Queue")
            print("0. Back to Main Menu")
            choice = input("Enter your choice: ")

            if choice == '1':
                value = input("Enter value to enqueue: ")
                self.queue.append(value)
                print(f"Value {value} enqueued to queue.")
            elif choice == '2':
                if not self.queue:
                    print("Queue is empty.")
                else:
                    value = self.queue.pop(0)
                    print(f"Value {value} dequeued from queue.")
            elif choice == '3':
                print("Queue:", self.queue)
            elif choice == '0':
                break
            else:
                print("Invalid choice. Please try again.")

    def binary_tree_menu(self):
        while True:
            print("\nBinary Tree Menu")
            print("1. Insert Value")
            print("2. Search Value")
            print("3. Update Value")
            print("4. Balance Tree")
            print("5. Visualize Tree")
            print("0. Back to Main Menu")
            choice = input("Enter your choice: ")

            if choice == '1':
                value = int(input("Enter value to insert: "))
                self.binary_tree.insert(value)
                print(f"Value {value} inserted into Binary Tree.")
            elif choice == '2':
                value = int(input("Enter value to search: "))
                node = self.binary_tree.search(value)
                if node:
                    print(f"Value {value} found in Binary Tree.")
                else:
                    print(f"Value {value} not found in Binary Tree.")
            elif choice == '3':
                old_value = int(input("Enter value to update: "))
                new_value = int(input("Enter new value: "))
                self.binary_tree.update(old_value, new_value)
            elif choice == '4':
                self.binary_tree.balance()
            elif choice == '5':
                self.binary_tree.visualize()
            elif choice == '0':
                break
            else:
                print("Invalid choice. Please try again.")

    def b_plus_tree_menu(self):
        while True:
            print("\nB+ Tree Menu")
            print("1. Insert Value")
            print("2. Visualize Tree")
            print("0. Back to Main Menu")
            choice = input("Enter your choice: ")

            if choice == '1':
                value = int(input("Enter value to insert: "))
                self.b_plus_tree.insert(value)
                print(f"Value {value} inserted into B+ Tree.")
            elif choice == '2':
                self.b_plus_tree.visualize()
            elif choice == '0':
                break
            else:
                print("Invalid choice. Please try again.")

    def graph_menu(self):
        while True:
            print("\nGraph Menu")
            print("1. Add Edge")
            print("2. BFS")
            print("3. DFS")
            print("4. Visualize Graph")
            print("0. Back to Main Menu")
            choice = input("Enter your choice: ")

            if choice == '1':
                u = input("Enter start vertex: ")
                v = input("Enter end vertex: ")
                self.graph.add_edge(u, v)
            elif choice == '2':
                start = input("Enter start vertex for BFS: ")
                bfs_order = list(nx.bfs_tree(self.graph, start).nodes)
                print("BFS Order: ", bfs_order)
            elif choice == '3':
                start = input("Enter start vertex for DFS: ")
                dfs_order = list(nx.dfs_tree(self.graph, start).nodes)
                print("DFS Order: ", dfs_order)
            elif choice == '4':
                self.visualize_graph()
            elif choice == '0':
                break
            else:
                print("Invalid choice. Please try again.")

    def visualize_graph(self):
        pos = nx.spring_layout(self.graph)
        nx.draw(self.graph, pos, with_labels=True, node_color='skyblue', node_size=2000, edge_color='black', font_size=10)
        plt.show()

    def dijkstra_menu(self):
        while True:
            print("\nDijkstra's Algorithm Menu")
            print("1. Add Edge with Weight")
            print("2. Update Edge Weight")
            print("3. Find Shortest Path")
            print("4. Visualize Graph")
            print("0. Back to Main Menu")
            choice = input("Enter your choice: ")

            if choice == '1':
                u = input("Enter start vertex: ")
                v = input("Enter end vertex: ")
                weight = int(input("Enter weight: "))
                self.graph.add_edge(u, v, weight=weight)
            elif choice == '2':
                u = input("Enter start vertex: ")
                v = input("Enter end vertex: ")
                if self.graph.has_edge(u, v):
                    weight = int(input("Enter new weight: "))
                    self.graph[u][v]['weight'] = weight
                    print(f"Edge ({u}, {v}) weight updated to {weight}.")
                else:
                    print(f"Edge ({u}, {v}) does not exist.")
            elif choice == '3':
                start = input("Enter start vertex: ")
                end = input("Enter end vertex: ")
                length, path = nx.single_source_dijkstra(self.graph, start, end)
                print(f"Shortest path from {start} to {end}: {path} with length {length}")
            elif choice == '4':
                self.visualize_graph()
            elif choice == '0':
                break
            else:
                print("Invalid choice. Please try again.")

if __name__ == "__main__":
    menu = Menu()
    menu.main_menu()
